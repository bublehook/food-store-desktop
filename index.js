const {app, BrowserWindow} = require('electron');
const serve = require('electron-serve');
 
const loadURL = serve({directory: './public'});
 
let mainWindow;
 
(async () => {
    await app.whenReady();
 
    mainWindow = new BrowserWindow({
        width : 1600,
        height: 900
    });
 
    await loadURL(mainWindow);
 
    // The above is equivalent to this:
    await mainWindow.loadURL('app://-');
    // The `-` is just the required hostname
})();